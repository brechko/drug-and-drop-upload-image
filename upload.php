<?php

function ocLog($filename, $data, $append=false)
    {
        if(!$append){
            file_put_contents(__DIR__ . '/logs/'. $filename . '_log.txt', var_export($data,true));
        }else{
            file_put_contents(__DIR__ . '/logs/'. $filename . '_log.txt', var_export($data,true).PHP_EOL, FILE_APPEND);
        }
        
    }

ocLog('SERVER', $_SERVER, false);
ocLog('POST', $_POST, false);
ocLog('FILES', $_FILES, false);


$uploaddir = getcwd().DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR;
$uploadfile = $uploaddir.basename($_FILES['file']['name']);

move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

?>