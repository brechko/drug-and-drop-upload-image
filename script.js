if (window.jQuery) {
    console.log('jQuery here!!!');
  }else{
      console.log('jQuery NOT here!!!');
  }


// Drug and drop file upload function
$(document).ready(function() {
    
    const dropZone = $('#dropZone'),
        maxFileSize = 1000000; // maximum file size - 1 mb.
    
    // Checking Browser Support
    if (typeof(window.FileReader) == 'undefined') {
        dropZone.text('Not supported by the browser!');
        dropZone.addClass('error');
    }
    
    // Add hover class on hover
    dropZone[0].ondragover = function() {
        dropZone.addClass('hover');
        return false;
    };
    
    // Removing the hover class
    dropZone[0].ondragleave = function() {
        dropZone.removeClass('hover');
        return false;
    };

    function uploadFile(file) {
        dropZone.removeClass('hover');
        dropZone.addClass('drop');

        // Checking the file size
        if (file.size > maxFileSize) {
            dropZone.text('The file is too big!');
            dropZone.addClass('error');
            return false;
        }
        
        // Create a request
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/upload.php');
        xhr.upload.addEventListener('progress', uploadProgress, false);
        xhr.onreadystatechange = stateChange;
        // xhr.setRequestHeader('X-FILE-NAME', file.name);
        var formData = new FormData();
        var image_file_name = file.name;
        formData.append('file', file, image_file_name);
        xhr.send(formData);
    }
    
    // Handling the Drop event
    dropZone[0].ondrop = function(event) {
        event.preventDefault();

        var file = event.dataTransfer.files[0];
        console.log('file');console.log(file);

        uploadFile(file);
    };

    $('#dropZoneInput').change(function(event) {
        event.preventDefault();

        var file = this.files[0];
        console.log('file');console.log(file);

        uploadFile(file);
    });
    
    // Show download percentage
    function uploadProgress(event) {
        var percent = parseInt(event.loaded / event.total * 100);
        dropZone.text('Uploading: ' + percent + '%');
    }
    
    // Post Handler
    function stateChange(event) {
        if (event.target.readyState == 4) {
            if (event.target.status == 200) {
                dropZone.text('Download completed successfully!');
            } else {
                dropZone.text('An error has occurred!');
                dropZone.addClass('error');
            }
        }
    }
    
});
// Drug and drop file upload function END

    // Создаем jQuery ajax запрос
// var formData = new FormData();
// var image_file_name = file.name;
// formData.append('file', file, image_file_name);
// $.ajax('/upload.php', {
//     method: 'POST',
//     data: formData,
//     processData: false,
//     contentType: false,
//     crossDomain: true,
//     success(data) {
//         console.log('success!');
//         console.log('data:');
//         console.log(data);
//     },
//     error() {
//         console.log('error uploadCropperImage');
//     },
// });